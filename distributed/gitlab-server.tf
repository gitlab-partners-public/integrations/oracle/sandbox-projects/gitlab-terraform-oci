resource "oci_core_instance" "gitlab-server" {
  count               = 2
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "gitlab-server-${count.index}"
  shape               = var.gitlab_server_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape_gitlab
    content {
      ocpus = var.gitlab_server_ocpus
      memory_in_gbs = var.gitlab_server_ram
    }
  }


  create_vnic_details {
    subnet_id              = local.private_subnet_id
    display_name           = "gitlab-server-${count.index}"
    assign_public_ip       = false
    hostname_label         = "gitlab-server-${count.index}"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.platform_image_id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      "export IS_GITLAB_SERVER=1",
      "export external_url=\"${local.external_url}\"",
      "export initial_passwd=\"${random_password.password.result}\"",
      "export access_key=\"${local.access_key}\"",
      "export secret_key=\"${local.secret_key}\"",
      "export smtp_user=\"${local.smtp_username}\"",
      "export smtp_passwd=\"${local.smtp_password}\"",
      "export smtp_domain=\"${local.private_subnet_domain_name}\"",
      "export home_region=\"${data.oci_identity_regions.home-region.regions[0].name}\"",
      "export namespace=\"${data.oci_objectstorage_namespace.ns.namespace}\"",
      "export private_subnet_cidr_env_var=\"${data.oci_core_subnet.gitlab_private_subnet.cidr_block}\"",
      "export postgres_server_ip_env_var=\"${oci_core_instance.postgres-server.private_ip}\"",
      "export redis_server_ip_env_var=\"${oci_core_instance.redis-server.private_ip}\"",
      "export gitaly_server1_ip_env_var=\"${oci_core_instance.gitaly-server[0].private_ip}\"",
      "export gitaly_server2_ip_env_var=\"${oci_core_instance.gitaly-server[1].private_ip}\"",
      "export bucket=\"${var.secrets_bucket}\"",
      "backup_upload_remote_directory=${var.backup_upload_remote_directory}; artifacts=${var.artifacts}",
      "external_diffs=${var.external_diffs}; lfs=${var.lfs}; uploads=${var.uploads}",
      "packages=${var.packages}; dependency_proxy=${var.dependency_proxy}; terraform_state=${var.terraform_state}",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}
