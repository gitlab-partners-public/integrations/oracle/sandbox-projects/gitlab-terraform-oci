resource "oci_core_instance" "redis-server" {
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "redis-server"
  shape               = var.redis_server_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape_redis
    content {
      ocpus = var.redis_server_ocpus
      memory_in_gbs = var.redis_server_ram
    }
  }


  create_vnic_details {
    subnet_id              = local.private_subnet_id
    display_name           = "redis-server"
    assign_public_ip       = false
    hostname_label         = "redis-server"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.platform_image_id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      "export IS_REDIS_SERVER=1",
      "export initial_passwd=\"${random_password.password.result}\"",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}
