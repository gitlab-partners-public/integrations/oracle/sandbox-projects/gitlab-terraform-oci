provider "oci" {
  alias  = "home_region"
  region = lookup(data.oci_identity_regions.home-region.regions[0], "name")
}


resource "oci_identity_dynamic_group" "ha_dynamic_group" {
  provider       = oci.home_region
  compartment_id = var.tenancy_ocid
  name           = "config_obj_store"
  description    = "Set up instance principle for HA config passing through obj storage"
  matching_rule  = "ALL {instance.compartment.id = '${var.compute_compartment_ocid}' }"

}

resource "oci_identity_policy" "ha_iam_policy" {
  provider       = oci.home_region
  name           = "config_obj_store"
  description    = "Allows object storage management via instance principle"
  compartment_id = var.compute_compartment_ocid
  statements = [
    "Allow dynamic-group ${oci_identity_dynamic_group.ha_dynamic_group.name} to manage buckets in compartment id ${var.compute_compartment_ocid}",
    "Allow dynamic-group ${oci_identity_dynamic_group.ha_dynamic_group.name} to manage objects in compartment id ${var.compute_compartment_ocid}"
  ]

}
