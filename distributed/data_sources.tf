data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.ad_number
}

data "template_file" "setup_gitlab-OL" {
  template = file("${path.module}/scripts/setup_omnibox-OL.sh")
}

data "oci_identity_tenancy" "tenancy" {
  tenancy_id = var.tenancy_ocid
}

data "oci_identity_regions" "home-region" {
  filter {
    name   = "key"
    values = [data.oci_identity_tenancy.tenancy.home_region_key]
  }
}

data "oci_objectstorage_namespace" "ns" {}

resource "random_password" "password" {
  length           = 16
  special          = false
  upper            = true
  lower            = true
  number           = true
}

data "oci_core_subnet" "gitlab_private_subnet" {
    subnet_id = local.private_subnet_id
}

data "oci_core_services" "test_services" {
}
