
resource "oci_objectstorage_bucket" "buckets" {
  for_each = toset( [var.backup_upload_remote_directory, var.artifacts, var.external_diffs, var.lfs, var.uploads, var.packages, var.dependency_proxy, var.terraform_state, var.secrets_bucket] )
  compartment_id = var.compute_compartment_ocid
  namespace      = data.oci_objectstorage_namespace.ns.namespace
  name           = each.key
  access_type    = "NoPublicAccess"
}
