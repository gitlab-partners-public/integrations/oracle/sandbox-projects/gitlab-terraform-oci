resource "oci_core_network_security_group" "simple_nsg" {
  #Required
  compartment_id = var.network_compartment_ocid
  vcn_id         = local.use_existing_network ? var.vcn_id : oci_core_vcn.gitlab_vcn.0.id

  #Optional
  display_name = "gitlab-nsg"

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

# Allow Egress traffic to all networks
resource "oci_core_network_security_group_security_rule" "simple_rule_egress" {
  network_security_group_id = oci_core_network_security_group.simple_nsg.id

  direction   = "EGRESS"
  protocol    = "all"
  destination = "0.0.0.0/0"

}

# Allow Ingress traffic to all networks
resource "oci_core_network_security_group_security_rule" "simple_rule_ingress" {
  network_security_group_id = oci_core_network_security_group.simple_nsg.id

  direction   = "INGRESS"
  protocol    = "all"
  source      = "0.0.0.0/0"

}
