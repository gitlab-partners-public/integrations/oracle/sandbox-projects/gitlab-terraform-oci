
###
# compute.tf outputs
###

output "URL_message" {
  value = "Load Balancer IP = ${oci_load_balancer_load_balancer.gitlab_lb.ip_address_details[0].ip_address}"
}

output "gitlab_url" {
  value = "${local.external_url}"
}

output "initial_root_password" {
  value = "${random_password.password.result}    (password for all applications (e.g. gitlab, gitaly, postgres))"
}

output "postgres_server" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.postgres-server.private_ip}"
}

output "redis_server" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.redis-server.private_ip}"
}

output "gitlab_server_1" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.gitlab-server[0].private_ip}"
}

output "gitlab_server_2" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.gitlab-server[1].private_ip}"
}

output "gitaly_server_1" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.gitaly-server[0].private_ip}"
}

output "gitaly_server_2" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.gitaly-server[1].private_ip}"
}

output "monitoring_server" {
  value = "ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@${oci_core_instance.bastion-server.public_ip}' opc@${oci_core_instance.prometheus-grafana-server.private_ip}"
}

