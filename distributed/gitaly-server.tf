resource "oci_core_instance" "gitaly-server" {
  count               = 2
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "gitaly-${count.index}"
  shape               = var.gitaly_server_shape


  dynamic "shape_config" {
    for_each = local.is_flex_shape_gitaly
    content {
      ocpus = var.gitaly_server_ocpus
      memory_in_gbs = var.gitaly_server_ram
    }
  }


  create_vnic_details {
    subnet_id              = local.private_subnet_id
    display_name           = "gitaly-${count.index}"
    assign_public_ip       = false
    hostname_label         = "gitaly-${count.index}"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type             = "image"
    source_id               = local.platform_image_id
    boot_volume_size_in_gbs = 1000
  }

#"./scripts/setup_omnibox.sh"
#(local.platform_image_os == "ubuntu" ? file(data.template_file.setup_gitlab.rendered) : ""),
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      "export IS_GITALY_SERVER=1",
      "export load_balancer_ip_env_var=\"${oci_load_balancer_load_balancer.gitlab_lb.ip_address_details[0].ip_address}\"",
      "export initial_passwd=\"${random_password.password.result}\"",
      "bucket=${var.secrets_bucket}",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}
