resource "oci_identity_smtp_credential" "gitlab_smtp_credential" {
    provider     = oci.home_region
    count       = local.create_smtp_credentials ? 1 : 0
    description = "gitlab_smtp"
    user_id     = var.current_user_ocid
}

resource "oci_email_sender" "gitlab_sender_server1" {
    compartment_id = var.compute_compartment_ocid
    email_address = "gitlab@${oci_core_instance.gitlab-server[0].hostname_label}.${local.private_subnet_domain_name}"
}

resource "oci_email_sender" "gitlab_sender_server2" {
    compartment_id = var.compute_compartment_ocid
    email_address = "gitlab@${oci_core_instance.gitlab-server[1].hostname_label}.${local.private_subnet_domain_name}"
}

resource "oci_email_sender" "noreply_sender_server1" {
    compartment_id = var.compute_compartment_ocid
    email_address = "no-reply@${oci_core_instance.gitlab-server[0].hostname_label}.${local.private_subnet_domain_name}"
}

resource "oci_email_sender" "noreply_sender_server2" {
    compartment_id = var.compute_compartment_ocid
    email_address = "no-reply@${oci_core_instance.gitlab-server[1].hostname_label}.${local.private_subnet_domain_name}"
}
