resource "oci_identity_customer_secret_key" "gitlab_customer_secret_key" {
    provider     = oci.home_region
    count        = local.create_access_keys ? 1 : 0
    display_name = "gitlab_keys"
    user_id      = var.current_user_ocid
}
