resource "oci_load_balancer_load_balancer" "gitlab_lb" {
  compartment_id = var.network_compartment_ocid 
  display_name   = "gitlab-LB"
  shape          = "flexible"
  subnet_ids     = [local.public_LB_subnet_id]
  is_private     = "false"
  network_security_group_ids = [oci_core_network_security_group.simple_nsg.id]

  shape_details {
      minimum_bandwidth_in_mbps = 10 
      maximum_bandwidth_in_mbps = 100
  }
}

resource "oci_load_balancer_backend_set" "gitlab_bes" {
  name             = "gitlab-bes"
  load_balancer_id = oci_load_balancer_load_balancer.gitlab_lb.id
  policy           = "ROUND_ROBIN"

  health_checker {
    port                = "80"
    protocol            = "TCP"
  }
}

resource "oci_load_balancer_backend" "gitlab-be" {
  load_balancer_id = oci_load_balancer_load_balancer.gitlab_lb.id
  backendset_name  = oci_load_balancer_backend_set.gitlab_bes.name
  ip_address       = element(oci_core_instance.gitlab-server.*.private_ip, count.index)
  port             = 80
  backup           = false
  drain            = false
  offline          = false
  weight           = 1

  count = 2
}

resource "oci_load_balancer_backend_set" "gitlab_grafana_bes" {
  name             = "gitlab-grafana-bes"
  load_balancer_id = oci_load_balancer_load_balancer.gitlab_lb.id
  policy           = "ROUND_ROBIN"

  health_checker {
    port                = "3000"
    protocol            = "TCP"
  }
}

resource "oci_load_balancer_backend" "gitlab-grafana-be" {
  load_balancer_id = oci_load_balancer_load_balancer.gitlab_lb.id
  backendset_name  = oci_load_balancer_backend_set.gitlab_grafana_bes.name
  ip_address       = oci_core_instance.prometheus-grafana-server.private_ip
  port             = 3000
  backup           = false
  drain            = false
  offline          = false
  weight           = 1
}

resource "oci_load_balancer_listener" "gitlab_listener_80" {
  load_balancer_id         = oci_load_balancer_load_balancer.gitlab_lb.id
  default_backend_set_name = oci_load_balancer_backend_set.gitlab_bes.name
  name                     = "gitlab-80"
  port                     = 80
  protocol                 = "HTTP"
  routing_policy_name      = oci_load_balancer_load_balancer_routing_policy.grafana_routing_policy_rule.name
  connection_configuration {
    idle_timeout_in_seconds = "10"
  }
}

resource "oci_load_balancer_load_balancer_routing_policy" "grafana_routing_policy_rule" {
  condition_language_version = "V1"
  load_balancer_id = oci_load_balancer_load_balancer.gitlab_lb.id
  name = "gitlab_grafana_RT"
  rules {
      actions {
          name = "FORWARD_TO_BACKENDSET"
          backend_set_name = oci_load_balancer_backend_set.gitlab_grafana_bes.name
      }
      condition = "any(http.request.url.path sw (i '/-/grafana'))" 
      name = "gitlab_grafana_rule1" 
  }  
}

resource "tls_private_key" "GitLabTLS" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "GitLabCert" {
  private_key_pem = tls_private_key.GitLabTLS.private_key_pem

  validity_period_hours = 26280
  early_renewal_hours   = 8760
  is_ca_certificate     = true
  allowed_uses          = ["cert_signing"]

  subject {
    common_name  = local.domain
    organization = "O"
  }
}

resource "oci_load_balancer_certificate" "GitLabLBCert" {
  load_balancer_id   = oci_load_balancer_load_balancer.gitlab_lb.id
  ca_certificate     = var.listener_ca_certificate == "" ? tls_self_signed_cert.GitLabCert.cert_pem : var.listener_ca_certificate
  certificate_name   = "GitLabCert"
  private_key        = var.listener_private_key == "" ? tls_private_key.GitLabTLS.private_key_pem : var.listener_private_key
  public_certificate = var.listener_public_certificate == "" ? tls_self_signed_cert.GitLabCert.cert_pem : var.listener_public_certificate

  lifecycle {
    create_before_destroy = true
  }
}

resource "oci_load_balancer_listener" "GitLabLBLsnr_SSL" {
  load_balancer_id         = oci_load_balancer_load_balancer.gitlab_lb.id
  name                     = "https"
  default_backend_set_name = "gitlab-bes"
  port                     = 443
  protocol                 = "HTTP"
  routing_policy_name      = oci_load_balancer_load_balancer_routing_policy.grafana_routing_policy_rule.name
  ssl_configuration {
    certificate_name        = oci_load_balancer_certificate.GitLabLBCert.certificate_name
    verify_peer_certificate = false
  }
}

