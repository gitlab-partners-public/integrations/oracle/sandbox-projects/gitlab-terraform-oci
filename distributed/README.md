# oci-gitlab

GitLab is a web-based DevOps platform that provides a Git-based repository management service, issue-tracking, and continuous integration and deployment (CI/CD) pipeline features. You can manage GitLab yourself and deploy to Oracle Cloud Infrastructure (OCI) for automating your cloud deployments. This module deploys the 1000-2000 user GitLab reference architecture into you OCI tenancy.

<!--  details of the architecture, see Deploy GitLab to enable CI/CD pipelines on OCI -->

# Reference Architecture

<img src="https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/raw/main/distributed/images/reference_architecture_2000.png" width="65%" height="65%">


# Prerequisites

1. Permission to ```manage``` the following types of resources in your Oracle Cloud Infrastructure tenancy: ```vcns```,```internet-gatewawys```,```service-gateways```,```NAT gateways```,```load-balancers```,```route-tables```,```routing-policies```,```subnets```,```object-storage-buckets```, ```email-approved-senders``` and ```instances```.

1. Quota to create the following resources: 1 VCN, 2 public subnets, 1 private subnet, 1 load balancer, 1 internet gateway, 1 NAT Gateway, 1 service gateway, 4 route table, 2 network security groups, 1 security list, 9 object storage buckets and 8 instances ( 1 bastion host, 2 GitLab nodes, 1 Redis node, 1 postgreSQL node, 2 Gitaly nodes and 1 monitoring node).

If you don't have the required permissions and quota, contact your tenancy administrator. See [Policy Reference](https://docs.cloud.oracle.com/en-us/iaas/Content/Identity/Reference/policyreference.htm), [Service Limits](https://docs.cloud.oracle.com/en-us/iaas/Content/General/Concepts/servicelimits.htm), [Compartment Quotas](https://docs.cloud.oracle.com/iaas/Content/General/Concepts/resourcequotas.htm).

# Deploy Using Oracle Resource Manager
1. Click [![Deploy to Oracle Cloud](https://oci-resourcemanager-plugin.plugins.oci.oraclecloud.com/latest/deploy-to-oracle-cloud.svg)](https://console.us-phoenix-1.oraclecloud.com/resourcemanager/stacks/create?region=home&zipUrl=https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/jobs/artifacts/main/raw/oci-gitlab-orm.zip?job=package_repo)


If you aren't already signed in, when prompted, enter the tenancy and user credentials.

2. Review and accept the terms and conditions.

3. Select the region where you want to deploy the stack.

4. Follow the on-screen prompts and instructions to create the stack.

5. After creating the stack, click Terraform Actions, and select Plan.

6. Wait for the job to be completed, and review the plan.

To make any changes, return to the Stack Details page, click Edit Stack, and make the required changes. Then, run the Plan action again.

7. If no further changes are necessary, return to the Stack Details page, click Terraform Actions, and select Apply.

The URL of the GitLab instance and the IP address of the nodes will appear at the end of the Apply Log:

<img src="https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/raw/main/distributed/images/Apply_output.png">

# Deploy Using the Terraform CLI

## Clone the repo
To deploy this reference architecture into your tenanacy using terraform CLI, first you'll need to clone th repo:

```
$> git clone https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci.git
$> cd gitlab-terraform-oci/distributed
```

## Prerequisites

First off, you'll need to do some pre-deploy setup. That's all detailed [here](https://github.com/cloud-partners/oci-prerequisites).

Secondly, create a ```terraform.tfvars``` file and populate with the following information:


```
## Terraform OCI provider required variables
user_ocid                = "<user_ocid>"
current_user_ocid        = "<user_ocid>"
fingerprint              = "<finger_print>"
private_key_path         = "<pem_private_key_path>"
tenancy_ocid             = "<tenancy_ocid>"
region                   = "<region>"
compute_compartment_ocid = "<compute_compartment>"
network_compartment_ocid = "<network_compartment>"
ssh_public_key           = "<ssh_public_key>"

## Region Specific platform ocid
platform_image_ocid      = "<platform_ocid"

## GitLab Server (optional)
#external_url="https://<mydomainname.com>"
```

## Create the Resources

Run the following commands:

```
$> terraform init
$> terraform plan
$> terraform apply
```

## Destroy the Deployment

When you no longer need the deployment, you can run this command to destroy the resources:

```
$> terraform destroy
```

# Testing

The easiest way to tell if you deployment works properly is to open a web browser and navigate to the ```gitlab_url```. Login with username ```root``` and ```<initial_root_password>``` specified on the termianl output. Try creating repository with a test README.md file. If your feeling ambitious you can try creating a branch and submitting a merge request. Here are some additional command line tests you can run from the various nodes in the deployment: 

### Testing Gitlab Access to Repository Storage
Confirm that Gitlab servers have access to the Gitaly storage nodes:
```
## SSH to a Gitlab server
$> ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@132.145.132.228' opc@10.0.2.6

## Run the test
$> sudo gitlab-rake gitlab:gitaly:check
```
```
Checking Gitaly ...

Gitaly: ... default ... OK
storage1 ... OK

Checking Gitaly ... Finished
```

### Testing Gitaly Servers
Confirm that Gitaly server can perform callbacks to the gitlab server API:
```
## SSH to a Gitaly server
$> ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@132.145.132.228' opc@10.0.2.5

## Run the test
$> sudo /opt/gitlab/embedded/bin/gitaly-hooks check  /var/opt/gitlab/gitaly/config.toml
```
```
Checking GitLab API access: OK
GitLab version: 13.9.4-ee
GitLab revision: 
GitLab Api version: v4
Redis reachable for GitLab: true
OK
```
### Testing Redis Server
Confirm Redis server is working correctlty by connecting to it and issueing the follwoing command:
```
## SSH to Redis Server
$> ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@132.145.132.228' opc@10.0.2.2

## Run the test
$> /opt/gitlab/embedded/bin/redis-cli -h <redis_server_ip> -a '<initial_root_password>' info replication
```
```
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
# Replication
role:master
connected_slaves:0
master_replid:b869c2fe40e771539b599e4ac1e73b63e3fb206e
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
```

### Test Backup Storage
Confirm the Gitlab nodes have access to Object Storage buckets for storing backups:
```
## SSH to a Gitlab server
$> ssh -i ~/.ssh/oci -o ProxyCommand='ssh -W %h:%p -i ~/.ssh/oci opc@132.145.132.228' opc@10.0.2.6

## Create a backup
$> sudo gitlab-rake gitlab:backup:create
```
```
[...]
Creating backup archive: 1616308870_2021_03_21_13.9.4-ee_gitlab_backup.tar ... done
Uploading backup archive to remote storage backup.bucket ... done
Deleting tmp directories ... done
[...]
Backup task is done.
```




