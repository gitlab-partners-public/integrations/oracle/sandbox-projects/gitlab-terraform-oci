resource "oci_core_instance" "prometheus-grafana-server" {
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "prometheus-grafana-server"
  shape               = var.monitoring_server_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape_monitoring
    content {
      ocpus = var.monitoring_server_ocpus
      memory_in_gbs = var.monitoring_server_ram
    }
  }


  create_vnic_details {
    subnet_id              = local.private_subnet_id
    display_name           = "monitor-server"
    assign_public_ip       = false
    hostname_label         = "monitor-server"
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.platform_image_id
  }

  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      "export IS_MONITORING_SERVER=1",
      "export initial_passwd=\"${random_password.password.result}\"",
      "export load_balancer_ip_env_var=\"${oci_load_balancer_load_balancer.gitlab_lb.ip_address_details[0].ip_address}\"",
      "export postgres_server_ip_env_var=\"${oci_core_instance.postgres-server.private_ip}\"",
      "export redis_server_ip_env_var=\"${oci_core_instance.redis-server.private_ip}\"",
      "export gitaly_server1_ip_env_var=\"${oci_core_instance.gitaly-server[0].private_ip}\"",
      "export gitaly_server2_ip_env_var=\"${oci_core_instance.gitaly-server[1].private_ip}\"",
      "export gitlab_server1_ip_env_var=\"${oci_core_instance.gitlab-server[0].private_ip}\"",
      "export gitlab_server2_ip_env_var=\"${oci_core_instance.gitlab-server[1].private_ip}\"",
      data.template_file.setup_gitlab-OL.rendered,
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}
