resource "oci_core_vcn" "gitlab_vcn" {
  count          = local.use_existing_network ? 0 : 1
  cidr_block     = var.gitlab_vcn_cidr_block
  dns_label      = var.gitlab_vcn_dns_label
  compartment_id = var.network_compartment_ocid
  display_name   = var.gitlab_vcn_dns_label

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#IGW
resource "oci_core_internet_gateway" "simple_internet_gateway" {
  count          = local.use_existing_network ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.gitlab_vcn[0].id
  enabled        = "true"
  display_name   = "gitlab-vcn-igw"

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#NAT Gateway
resource "oci_core_nat_gateway" "gitlab_private_subnet_nat_gateway" {
    count          = local.use_existing_network ? 0 : 1
    compartment_id = var.network_compartment_ocid
    vcn_id         = oci_core_vcn.gitlab_vcn[0].id
    display_name   = "gitlab-vcn-NATgw"
}

#Service Gateway
resource "oci_core_service_gateway" "gitlab_service_gateway" {
    count          = local.use_existing_network ? 0 : 1
    compartment_id = var.network_compartment_ocid
    vcn_id         = oci_core_vcn.gitlab_vcn[0].id
    services {
        service_id = data.oci_core_services.test_services.services.0.id
    }
    display_name   = "gitlab-vcn-ServiceGW"
}

#simple public subnet
resource "oci_core_subnet" "bastion_public_subnet" {
  count                      = local.use_existing_network ? 0 : 1
  cidr_block                 = var.bastion_public_subnet_cidr_block
  compartment_id             = var.network_compartment_ocid
  vcn_id                     = oci_core_vcn.gitlab_vcn[0].id
  display_name               = var.bastion_public_subnet_dns_label
  dns_label                  = var.bastion_public_subnet_dns_label
  prohibit_public_ip_on_vnic = false

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#simple public subnet
resource "oci_core_subnet" "LB_public_subnet" {
  count                      = local.use_existing_network ? 0 : 1
  cidr_block                 = var.LB_public_subnet_cidr_block
  compartment_id             = var.network_compartment_ocid
  vcn_id                     = oci_core_vcn.gitlab_vcn[0].id
  display_name               = var.LB_public_subnet_dns_label
  dns_label                  = var.LB_public_subnet_dns_label
  prohibit_public_ip_on_vnic = false

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

#simple private subnet
resource "oci_core_subnet" "gitlab_private_subnet" {
  count                      = local.use_existing_network ? 0 : 1
  cidr_block                 = var.gitlab_private_subnet_cidr_block 
  compartment_id             = var.network_compartment_ocid
  vcn_id                     = oci_core_vcn.gitlab_vcn[0].id
  display_name               = var.gitlab_private_subnet_dns_label
  dns_label                  = var.gitlab_private_subnet_dns_label
  prohibit_public_ip_on_vnic = true

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

resource "oci_core_route_table" "public_route_table_bastion" {
  count          = local.use_existing_network ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.gitlab_vcn[0].id
  display_name   = "bastion-public-rt"

  route_rules {
    network_entity_id = oci_core_internet_gateway.simple_internet_gateway[count.index].id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

resource "oci_core_route_table" "public_route_table_loadbalancer" {
  count          = local.use_existing_network ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.gitlab_vcn[0].id
  display_name   = "load-balancer-public-rt"

  route_rules {
    network_entity_id = oci_core_internet_gateway.simple_internet_gateway[count.index].id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

resource "oci_core_route_table" "private_route_table" {
  count          = local.use_existing_network ? 0 : 1
  compartment_id = var.network_compartment_ocid
  vcn_id         = oci_core_vcn.gitlab_vcn[0].id
  display_name   = "gitlab-private-rt"

  route_rules {
    network_entity_id = oci_core_nat_gateway.gitlab_private_subnet_nat_gateway[count.index].id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }
  
  route_rules {    
    network_entity_id = oci_core_service_gateway.gitlab_service_gateway[count.index].id
    destination       = data.oci_core_services.test_services.services[0].cidr_block
    destination_type = "SERVICE_CIDR_BLOCK"
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}

resource "oci_core_route_table_attachment" "route_table_attachment_public_bastion" {
  count          = local.use_existing_network ? 0 : 1
  subnet_id      = local.public_bastion_subnet_id 
  route_table_id = oci_core_route_table.public_route_table_bastion[count.index].id
}

resource "oci_core_route_table_attachment" "route_table_attachment_public_LB" {
  count          = local.use_existing_network ? 0 : 1
  subnet_id      = local.public_LB_subnet_id
  route_table_id = oci_core_route_table.public_route_table_loadbalancer[count.index].id
}

resource "oci_core_route_table_attachment" "route_table_attachment_private" {
  count          = local.use_existing_network ? 0 : 1
  subnet_id      = local.private_subnet_id 
  route_table_id = oci_core_route_table.private_route_table[count.index].id
}

