# gitlab-terraform-oci [![Deploy to Oracle Cloud](https://oci-resourcemanager-plugin.plugins.oci.oraclecloud.com/latest/deploy-to-oracle-cloud.svg)](https://console.us-phoenix-1.oraclecloud.com/resourcemanager/stacks/create?region=home&zipUrl=https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/jobs/artifacts/main/raw/oci-gitlab-orm.zip?job=package_repo)

The [Oracle Cloud Infrastructure (OCI) Quick Start](https://github.com/oracle-quickstart?q=oci-quickstart) is a collection of examples that allow Oracle Cloud Infrastructure users to get a quick start deploying advanced infrastructure on OCI.

*gitlab-terraform-oci* contains the Terraform template that can be used for deploying GitLab self-managed to Oracle Cloud Infrastructure from local Terraform CLI, [OCI Resource Manager](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) and [OCI Cloud Shell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm).

*gitlab-terraform-oci* repository contains the following folders:

* [standalone](standalone/README.md) --> deploys GitLab as a standalone Compute instance supporting up to 1000 users.
* [distributed](distributed/README.md) --> deploys GitLab as distributed Compute instances supporting up to 2000 users.
* [gitlab-runner](gitlab-runner/README.md) --> deploys GitLab Runners

GitLab is installed on the Compute instance using [Omnibus GitLab](https://docs.gitlab.com/omnibus/installation/) Enteprise Edition, which comes with GitLab Free tier enabled, containing the same features that are availble in the Community Edition distribution and enable you to upgrade to Enterprise Edition features without requiring any down time. More information available [here](https://about.gitlab.com/install/ce-or-ee/).

This repo is under active development.  Building open source software is a community effort.  We're excited to engage with the community building this.

## Prerequisites

First off we'll need to do some pre deploy setup.  That's all detailed [here](https://github.com/oracle/oci-quickstart-prerequisites).

## Resource Manager Deployment

This Quick Start uses [OCI Resource Manager](https://docs.cloud.oracle.com/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) to make deployment quite easy.
You can click on the [![Deploy to Oracle Cloud](https://oci-resourcemanager-plugin.plugins.oci.oraclecloud.com/latest/deploy-to-oracle-cloud.svg)](https://console.us-phoenix-1.oraclecloud.com/resourcemanager/stacks/create?region=home&zipUrl=https://gitlab.com/gitlab-com/alliances/oracle/sandbox-projects/gitlab-terraform-oci/-/jobs/artifacts/main/raw/oci-gitlab-orm.zip?job=package_repo) button to automatically create your ORM Stack from the latest published zip file. Then you can select which Stack you want to deploy from the *Working Directory* selector.

For more information about each deployment, check the `README.md` file on each individual folder:

* [standalone](standalone/README.md)
* [distributed](distributed/README.md)
* [gitlab-runner](gitlab-runner/README.md)
