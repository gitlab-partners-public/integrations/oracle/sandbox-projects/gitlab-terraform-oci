data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.ad_number
}

data "oci_core_subnet" "gitlab_subnet" {
    subnet_id = local.use_existing_network ? var.subnet_id : oci_core_vcn.simple[0].id
}

// data "oci_core_images" "oracle_linux_79" {
//   compartment_id   = var.tenancy_ocid
//   operating_system = "Oracle Linux"
//   sort_by          = "TIMECREATED"
//   sort_order       = "DESC"
//   state            = "AVAILABLE"

//   # filter restricts to pegged version regardless of region
//   filter {
//     name = "operating_system_version"
//     #e.g. 16.04 or 18.04 or 20.04, etc
//     values = ["7.9"]
//     regex  = false
//   }
// }

// resource "random_password" "password" {
//   length           = 16
//   special          = false
//   upper            = true
//   lower            = true
//   number           = true
// }
