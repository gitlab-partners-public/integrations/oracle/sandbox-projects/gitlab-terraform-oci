data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = var.ad_number
}

data "oci_core_subnet" "gitlab_subnet" {
    subnet_id = local.use_existing_network ? var.subnet_id : oci_core_vcn.simple[0].id
}

// data "oci_core_images" "ubuntu" {
//   compartment_id   = var.tenancy_ocid
//   operating_system = "Canonical Ubuntu"
//   sort_by          = "TIMECREATED"
//   sort_order       = "DESC"
//   state            = "AVAILABLE"

//   # filter restricts to pegged version regardless of region
//   filter {
//     name = "operating_system_version"
//     #e.g. 16.04 or 18.04 or 20.04, etc
//     values = [local.ubuntu_version]
//     regex  = false
//   }

//   # filter restricts to ubuntu_release-yyyy.mm
//   filter {
//     name   = "display_name"
//     values = ["-${local.ubuntu_version}-\\d\\d\\d\\d\\.\\d\\d."]
//     regex  = true
//   }
// }

