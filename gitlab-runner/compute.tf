resource "oci_core_instance" "simple-vm" {
  count               = var.number_of_runners
  availability_domain = local.availability_domain
  compartment_id      = var.compute_compartment_ocid
  display_name        = "${var.vm_display_name}-${count.index}"
  shape               = local.compute_shape

  dynamic "shape_config" {
    for_each = local.is_flex_shape
    content {
      ocpus         = local.flex_shape_ocpus
      memory_in_gbs = local.flex_shape_memory
    }
  }


  create_vnic_details {
    subnet_id              = local.use_existing_network ? var.subnet_id : oci_core_subnet.simple_subnet[0].id
    display_name           = "${var.subnet_display_name}-${count.index}"
    assign_public_ip       = local.is_public_subnet
    hostname_label         = local.use_hostnames ? "${var.hostname_label}-${count.index}" : null
    skip_source_dest_check = false
    nsg_ids                = [oci_core_network_security_group.simple_nsg.id]
  }

  source_details {
    source_type = "image"
    source_id   = local.image_id
  }

  #"./scripts/setup_omnibox.sh"
  #(local.platform_image_os == "ubuntu" ? file(data.template_file.setup_gitlab.rendered) : ""),
  metadata = {
    ssh_authorized_keys = var.ssh_public_key
    user_data = base64encode(join("\n", list(
      "#!/usr/bin/env bash",
      "set -x",
      (data.template_file.install_runner_ol8.rendered),
    )))
  }

  freeform_tags = map(var.tag_key_name, var.tag_value)
}


data "template_file" "install_runner_ol8" {
  template = file("${path.module}/scripts/install-gitlab-runner-docker-ol8.sh")

  vars = {
    gitlab_server_url                = var.gitlab_server_url
    gitlab_runner_registration_token = var.gitlab_runner_registration_token
    gitlab_runner_docker_image       = var.gitlab_runner_docker_image
    gitlab_runner_tag_list           = var.gitlab_runner_tag_list
    gitlab_runner_docker_priv_flag   = local.gitlab_runner_docker_priv_flag
  }
}
